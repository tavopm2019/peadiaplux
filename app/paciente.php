<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paciente extends Model
{
    //
    protected $table = 'paciente'; //como esta en la base
    protected $primaryKey ='numeropac';
    //protected $keyType = 'int'; //si la llave no es un entero
    public $timestamps = false;
    //protected $fillable =['numeropac','nombre1','nombre2','apellido1','apellido2','sexo','edad','historia'];

}
