<?php

namespace App\Http\Controllers;

use App\doctores;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Tests\DependencyInjection\ClassNotInContainer;

class DoctoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $doctores = doctores::all();
        return view('menu1', compact('doctores'));
    }
}
