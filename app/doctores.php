<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctores extends Model
{
    //
    protected $table = 'doctores'; //como esta en la base
    protected $primaryKey ='numerodoc';
    //protected $keyType = 'int'; //si la llave no es un entero
    public $timestamps = false;
    //protected $fillable =['numerodoc','nombre','especialidad','usuario','contra'];

}
