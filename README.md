# Pediatric PEDIAPLUX

## Overview

Desarrollar una aplicación web, que permita llevar el control de los pacientes vistos por el pediatra en turno, tanto desarrollando, una consulta llevar la historia clínica, y poder ingresar los datos de los exámenes, incluyendo un formulario para las fórmulas que son muy extensas, también control de vacunas, tiene que lograr guardar la informacion de cada paciente, y poder presentarlas en una forma práctica y simple. 

Fue realizado para presentar una actualizacion ya que todo se lleva a mano en el mismo. 

## Proposito

Ayudar a mejorar el ingreso y el control de pacientes de los hospitales de Guatemala públicos, ya que se ha dado cuenta de que llevan todo a mano y eso permite errores, tanto de duplicidad de datos, como errores de cálculo humano. 

## Alcance

1. Alcance Medicinal
2. público en general
3. sector público/ privado. 

## Restricciones
1. Plataforma web principal
2. Diseño moderno ui 

## Requerimientos específicos: 
1. Internet. 
2. Aplicación desarrollada con Php y HTML 5. 
3. Aplicación personalizada con CSS. 
4. Aplicación con uso de Bootstrap. 
5. Aplicación adaptativa para el tipo de pantalla donde se muestre. 
6. Base de datos relacional con Mysql. 
7. Manejo de usuarios. 
8. Manejo de cuenta. 
9. Manejo de datos de paciente: 
    1. Capacidad de manejo de historial clínica del paciente 
    2. Capacidad de manejo e historial del crecimiento del paciente. 
    
## Requerimientos: 
1. El programa debe poder almacenar la informacion del historial de caso clínico de cada paciente. 
2. EL programa no debe permitir duplicidad de ingresos. 
3. El programa debe de llevar una secuencia, para el correcto uso. 
4. El programa tiene que ser intuitivo en el uso, para su correcto uso por los doctores, que están en turno. 
5. El usuario pediatra, tiene que poder crear historias, editarlas. Mas no puede borrar. 
6. Solo existe una historia clínica por paciente. 
7. El pediatra tiene que poder acceder al formulario de datos básicos de una forma intuitiva. 
8. El programa tiene que desarrollar un resumen general de los pacientes del pediatra

 
## Creditos 
Gustavo Pereira  08000114 

## Link
[pediaplux](www.pediaplux.com)
