<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('principal');
});

Route::get('/home2', 'DoctoresController@index');
Route::get('/pac', 'PacienteController@index');

Route::get('/home', function () {
    return view('menu1');
});

Route::get('/historial', function () {
    return view('HistorialPaciente');
});

Route::get('/insertar', function () {
    return view('InsertPaciente');
});

Route::get('/revisar', function () {
    return view('RevisarPaciente');
});

Route::get('/formulario', function () {
    return view('FormulasPaciente');
});

Route::get('/salir', function () {
    return view('principal');
});

Route::post('/ingresar', 'PacienteController@store');
//Route::get('/show/{id}', 'PacienteController@show');
Route::post('/show','PacienteController@show');


