<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <link rel="canonical" href="https://getbootstrap.com/docs/3.4/examples/dashboard/">

    <title>Dashboard Template for Bootstrap</title>


    <!-- Custom styles for this template -->
    <LINK href="stilos/Dashboard.css" rel="stylesheet" type="text/css">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>



    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Mediplux</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Salir</a></li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="/home">Home</a></li>
                <li><a href="/insertar">Nuevo PX</a></li>
                <li class="active"><a href="/revisar">Revisar PX<span class="sr-only">(current)</span></a></li>
                <li><a href="/historial">Historial PX<</a></li>
                <li><a href="/formulario">Formulario</a></li>
                <li><a href="/salir">Salir</a></li>
            </ul>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <img class="img-responsive" src="ads/logo.png" alt="Chania">
            <h1 class="page-header">REVISION DE PACIENTE</h1>

            <h2 class="sub-header">Doctora, Esthefany Quiñonez</h2>

            <h2 class="sub-header">Datos del PX</h2>
            <form class="center-block" >
                <div class="form-group form-inline">
                    <img class="img-responsive" src="ads/Pefil.PNG" alt="Chania">
                </div>
                <div class="form-group form-inline "> <!-- Full Name -->
                    <label for="full_name_id" class="control-label">Nombre:</label>
                    @foreach($paciente as $pac)
                    <label for="full_name_id" class="control-label" id="nombrex" name="nombrex">{{$pac->nombre1}}</label>
                    <label for="full_name_id" class="control-label" id="nombre2" name="nombre2">{{$pac->nombre2}}</label>
                </div>
                <div class="form-group form-inline "> <!-- Full Name -->
                    <label for="full_name_id" class="control-label">Apellidos:</label>
                    <label for="full_name_id" class="control-label" id="Apellido1" name="Apellido1">{{$pac->apellido1}}</label>
                    <label for="full_name_id" class="control-label" id="Apellido2" name="Apellido2">{{$pac->apellido2}}</label>
                </div>
                <div class="form-group form-inline "> <!-- Full Name -->
                    <label for="full_name_id" class="control-label">Sexo: </label>
                    <label for="full_name_id" class="control-label" id="sexo" name="sexo">{{$pac->sexo}}</label>
                    <label for="full_name_id" class="control-label">Edad: </label>
                    <label for="full_name_id" class="control-label" id="edad" name="edad">{{$pac->edad}}</label>
                </div>


                <h2 class="sub-header">Historial</h2>
                <div class="form-group form-inline"> <!-- Full Name -->
                    <label for="comment" class="control-label">Actual: Historia Clinica</label><br>
                    <textarea class="form-control" rows="5" cols="150" id="historia" name="historia" disabled>{{$pac->historia}}</textarea>
                    <label for="comment" class="control-label">Actualizacion: Historia Clinica</label>
                    <textarea class="form-control" rows="5" cols="150" id="nuevahistoria" name="nuevahistoria"></textarea>
                </div>
                @endforeach


                <div class="form-group"> <!-- Submit Button -->
                    <button type="submit" class="btn btn-primary">Ingresar</button>
                </div>

            </form>



        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../dist/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="../../assets/js/vendor/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
