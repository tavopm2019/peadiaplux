<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <link rel="canonical" href="https://getbootstrap.com/docs/3.4/examples/dashboard/">

    <title>Dashboard Template for Bootstrap</title>


    <!-- Custom styles for this template -->
    <LINK href="stilos/Dashboard.css" rel="stylesheet" type="text/css">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>



    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Mediplux</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Salir</a></li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <ul class="nav nav-sidebar">
                    <li class="active"><a href="/home">Home</a></li>
                    <li><a href="/insertar">Nuevo PX</a></li>
                    <li><a data-toggle="modal" data-target="#exampleModal" disable>Revisar PX<span class="sr-only">(current)</span></a></li>
                    <li><a href="/pac">Historial PX</a></li>
                    <li><a href="/formulario">Formulario</a></li>
                    <li><a href="/salir">Salir</a></li>
                </ul>
            </ul>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <img class="img-responsive" src="ads/logo.png" alt="Chania">
            <h1 class="page-header">INGRESO DE PACIENTE</h1>

            <h2 class="sub-header">Doctora, Esthefany Quiñonez</h2>

            <h2 class="sub-header">Datos del PX</h2>
            <form class="center-block" action="/ingresar" method="post">
                {{csrf_field()}}

                <div class="form-group form-inline col-xs-3"> <!-- Full Name -->
                    <label for="full_name_id" class="control-label">Nombre 1</label>
                    <input  type="text" class="form-control"  id="nombre1" name="nombre1" placeholder="" >
                    <label for="street1_id" class="control-label ">Nombre 2</label>
                    <input type="text" class="form-control" id="nombre2" name="nombre2" placeholder="">
                </div>
                <div class="form-group form-inline col-xs-3"> <!-- Full Name -->
                    <label for="full_name_id" class="control-label">Apellido 1</label>
                    <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="">
                    <label for="street1_id" class="control-label ">Apellido  2</label>
                    <input type="text"class="form-control" id="apellido2" name="apellido2" placeholder="">
                </div>
                <div class="form-group form-inline  col-xs-6"> <!-- Full Name -->
                    <label for="state_id" class="control-label">Sexo</label>
                    <select class="form-control" id="sexo" name="sexo">
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select>
                    <br><br>
                    <label for="state_id" class="control-label">Edad</label>
                    <select class="form-control" id="edad" name="edad">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">19</option>
                    </select>

                </div>
                <br><br><br><br><br><br>

                <h2 class="sub-header">Subir foto</h2>
                <div class="form-group form-inline" disabled>
                    <label class="custom-file-label form-control" for="inputGroupFile01">Choose file</label>
                        <span class="input-group-text form-control" id="inputGroupFileAddon01">Upload</span>
                        <input type="file" class="custom-file-input form-control" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                </div>

                <h2 class="sub-header">Historial</h2>
                <div class="form-group form-inline"> <!-- Full Name -->
                    <label for="comment" class="control-label">Historia Clinica</label>
                    <textarea class="form-control" rows="5" cols="150" id="historia" name="historia"></textarea>
                </div>


                <div class="form-group"> <!-- Submit Button -->
                    <button type="submit" class="btn btn-primary">Ingresar</button>
                </div>

            </form>



        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ingrese el codigo de paciente interno</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/show">
                        {{csrf_field()}}
                        <input type="text" name="codigo" value="" id = "codigo"/>
                        <input type="submit" value="enviar" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../dist/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="../../assets/js/vendor/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
